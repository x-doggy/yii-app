<?php

/**
 * This is the model class for table "predmege.predm_ege".
 *
 * The followings are the available columns in table 'predmege.predm_ege':
 * @property string $ID
 * @property string $NAME
 * @property double $ORDER_EGE
 * @property string $WHEN_CHANGED
 * @property string $WHEN_CREATED
 * @property string $WHO_CHANGED
 * @property string $WHO_CREATED
 * @property double $MIN_BALL
 *
 * The followings are the available model relations:
 * @property PREDM[] $PREDMs
 */
class PredmEge extends CActiveRecord
{
	/**'search'
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'predmege.predm_ege';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NAME, ORDER_EGE, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED', 'required'),
			array('ORDER_EGE, MIN_BALL', 'numerical'),
			array('NAME', 'length', 'max'=>50),
			array('WHO_CHANGED, WHO_CREATED', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NAME, ORDER_EGE, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED, MIN_BALL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NAME' => 'Name',
			'ORDER_EGE' => 'Order Ege',
			'WHEN_CHANGED' => 'When Changed',
			'WHEN_CREATED' => 'When Created',
			'WHO_CHANGED' => 'Who Changed',
			'WHO_CREATED' => 'Who Created',
			'MIN_BALL' => 'Min Ball',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('ORDER_EGE',$this->ORDER_EGE);
		$criteria->compare('WHEN_CHANGED',$this->WHEN_CHANGED,true);
		$criteria->compare('WHEN_CREATED',$this->WHEN_CREATED,true);
		$criteria->compare('WHO_CHANGED',$this->WHO_CHANGED,true);
		$criteria->compare('WHO_CREATED',$this->WHO_CREATED,true);
		$criteria->compare('MIN_BALL',$this->MIN_BALL);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbMaria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PredmEge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
