<?php

class PredmEgeController extends Controller {

  public function actionIndex() {
    $model = new PredmEge;
    if (isset($_POST['PredmEge'])) {
      $model->attributes = $_POST['PredmEge'];
//       if ($model->save()) {
//         $this->redirect(array('view', 'id' => $model->ID));
//       }
    }

    $dbOrclList = self::mapToSelectOptions(PredmEge::model(), array(
                'select' => 'NAME',
                'order' => 'ID ASC',
    ));
    $this->render('index', array(
        'dbOrclList' => $dbOrclList,
        'model' => $model,
    ));
  }
  
  public function actionForm() {
    $model = new PredmEge;
    if (isset($_POST['PredmEge'])) {
      $model->attributes = $_POST['PredmEge'];
    }
    $this->render('form', array(
        'model' => $model,
    ));
  }
  
  // Uncomment the following methods and override them if needed
  /*
    public function filters()
    {
    // return the filter configuration for this controller, e.g.:
    return array(
    'inlineFilterName',
    array(
    'class'=>'path.to.FilterClass',
    'propertyName'=>'propertyValue',
    ),
    );
    } */

  public function actions() {
    // return external action classes, e.g.:
    return array(
        'page' => array(
            'class' => 'CViewAction',
        ),
    );
  }

  private static function mapToSelectOptions($model, array $crit) {
    $data = $model->findAll($crit);
    return array_values(
            array_map(function ($item) {
              return $item['NAME'];
            }, $data)
    );
  }

}
