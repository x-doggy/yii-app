<?php

// Uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'x-doggy\'s Yii try app',

    // preloading
    'preload' => array(
        'log',
        'debug',
    ),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'bootstrap.behaviors.*',
        'bootstrap.components.*',
        'bootstrap.form.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
    ),

    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'daz',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1'),
            'generatorPaths' => array('bootstrap.gii'),
        ),
    ),

    'aliases' => array(
        'bootstrap' => 'ext.yiistrap',
        'vendor.twbs.bootstrap.dist' => 'ext.twbs.bootstrap.dist',
    ),

    // application components
    'components'=>array(

        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),

        // uncomment the following to enable URLs in path-format
        /*
            'urlManager'=>array(
                'urlFormat'=>'path',
                'rules'=>array(
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),
        */

        // database settings are configured in database.php
        'db'=>require(dirname(__FILE__).'/database.php'),

        // Oracle database
        'dbOracle'=>require(dirname(__FILE__) . '/databaseOracle.php'),

         // Mariadb database
        'dbMaria'=>require(dirname(__FILE__) . '/databaseMaria.php'),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>YII_DEBUG ? null : 'site/error',
        ),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                //array(
                //    'class'=>'CWebLogRoute',
                //),
            ),
        ),

        'bootstrap' => array(
            'class' => 'ext.yiistrap.components.TbApi',
        ),

        'debug' => array(
            'class' => 'ext.yii2-debug.Yii2Debug',
            'enabled' => true,
            'showConfig' => true,
            'highlightCode' => true,
        ),

        'clientScript' => array(),

    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'x-doggy@ya.ru',
    ),
);
