<?php

    /* Oracle database config */

    return array(
        'class'=>'ext.oci8pdo.OciDbConnection',
        'connectionString'=>'oci:dbname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.11.36)(PORT=1521))(CONNECT_DATA=(SID=student)));charset=AL32UTF8;',
        'username' => 'PRACT',
        'password' => 'practicant',
        'enableProfiling' => true,
        'enableParamLogging' => true,
    );
