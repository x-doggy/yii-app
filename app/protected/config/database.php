<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	'connectionString' => 'mysql:host=localhost;dbname=yii1app',
	'emulatePrepare' => true,
	'username' => 'x-doggy',
	'password' => 'daz',
	'charset' => 'utf8',
	'enableProfiling' => true,
	'enableParamLogging' => true,
);
