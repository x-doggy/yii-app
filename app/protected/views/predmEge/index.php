<?php
/* @var $this PredmEgeController */

$this->breadcrumbs = array(
    'Средние оценки',
);

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/predmege.js', CCLientScript::POS_END);

$cnt = 0;
?>

<div class="form">
  <?php
  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
      'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
  ));
  ?>
  <fieldset>
    <legend>Средние оценки абитуриента</legend>

    <div class="centeraligned">
      <?php echo TbHtml::label('Введите № абитуриента:', 'abitId'); ?>
      <?php
      echo TbHtml::textField('abitId', '', array(
          'class' => 'form-next-focus form-next-focus--first',
          'maxlength' => '3',
          'pattern' => '[0-9]*',
      ));
      ?>
    </div>


    <div id="toggled">
      <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Поля с отсутствующими предметами оставить пустыми!'); ?>
      <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Для перемещения на предыдущее поле ввода используйте Shift+Tab.'); ?>
      <table id="table-next-toggle">
        <?php foreach ($dbOrclList as $key => $val): ?>
          <tr class="form-group row">
            <td><?php echo ++$cnt, '.'; ?></td>
            <td><?php echo $val; ?></td>
            <td>
              <?php
              echo TbHtml::textField("predm$key", '', array(
                  'maxlength' => '1',
                  'class' => 'form-control form-next-input form-next-focus',
                  'pattern' => '[2-5]',
              ));
              ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>

      <?php
      $this->widget('bootstrap.widgets.TbModal', array(
          'id' => 'avg-modal',
          'header' => 'Средняя оценка',
          'content' => '<div id="avg-result"></div>',
          'footer' => array(
              TbHtml::button('Save Changes', array(
                  'data-dismiss' => 'modal',
                  'color' => TbHtml::BUTTON_COLOR_PRIMARY,
                  'id' => 'modal-btn--save',
              )),
              TbHtml::button('Close', array(
                  'data-dismiss' => 'modal',
                  'id' => 'modal-btn--close',
              )),
          ),
      ));
      ?>

      <?php
      echo TbHtml::button('Посчитать среднюю оценку', array(
          'style' => TbHtml::BUTTON_COLOR_PRIMARY,
          'size' => TbHtml::BUTTON_SIZE_DEFAULT,
          'data-toggle' => 'modal',
          'data-target' => '#avg-modal',
          'id' => 'btnToCalc',
          'class' => 'form-next-focus',
      ));
      ?>

    </div>

  </fieldset>
  <?php $this->endWidget(); ?>
</div><!-- form --> 
