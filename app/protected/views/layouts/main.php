<?php 
    /* @var $this Controller */
    $encodedTitle = CHtml::encode($this->pageTitle);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <!-- Yiistrap -->
    <?php Yii::app()->bootstrap->register(); ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/styles-twbs-additional.css'); ?>

    <title><?php echo $encodedTitle; ?></title>
</head>

<body>

<div class="x-container" id="page">

    <header class="subhead" id="header">
        <?php $this->widget('bootstrap.widgets.TbNavbar', array(
            'color' => TbHtml::NAVBAR_COLOR_INVERSE,
            'brandLabel' => $encodedTitle,
            'display' => null, // default is static to top
            'collapse' => true,
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbNav',
                    'items' => array(
                        array('label'=>'Главная', 'url'=>array('/site/index')),
                        array('label'=>'О сайте', 'url'=>array('/site/page', 'view'=>'about')),
                        array('label'=>'Средние оценки', 'url'=>array('/PredmEge/index')),
                        array('label'=>'Логин', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Логаут ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)        
                    ),
                ),
            ),
        )); ?>

    </header><!-- header -->

    <?php if (isset($this->breadcrumbs)) :?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
            'links' => $this->breadcrumbs,
        )); ?>
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>
    <footer class="footer centeraligned" id="footer">
        <div>Копирайт &copy; <?php echo date('Y'); ?>, <a href="https://gitlab.com/x-doggy" target="_blank">Владимир Стадник</a>.</div>
        <div>Всё права зарезервированы.</div>
        <div><?php echo Yii::powered(); ?></div>
        <div>Yii версии <?php echo Yii::getVersion(); ?></div>
    </footer><!-- footer -->

</div><!-- page -->

</body>
</html>
