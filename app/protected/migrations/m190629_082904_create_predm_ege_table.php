<?php

class m190629_082904_create_predm_ege_table extends CDbMigration
{
	public function up()
	{
    $this->createTable('predm_ege', array(
      'ID' => 'pk',
      'NAME' => 'string',
      'ORDER_EGE' => 'double',
      'WHEN_CHANGED' => 'string',
      'WHEN_CREATED' => 'string',
      'WHO_CHANGED' => 'string',
      'WHO_CREATED' => 'string',
      'MIN_BALL' => 'double',
    ));
    $this->insertMultiple(array(
      array(
        'ID' => 1,
        'NAME' => 'Vladimir Stadnik',
        'ORDER_EGE' => 4.34,
        'WHEN_CHANGED' => '2018',
        'WHEN_CREATED' => '2019',
        'WHO_CHANGED' => '2019',
        'WHO_CREATED' => '2018',
        'MIN_BALL' => 3
      ),
      array(
        'ID' => 2,
        'NAME' => 'Bogdan Turovets',
        'ORDER_EGE' => '4.4',
        'WHEN_CHANGED' => '2018',
        'WHEN_CREATED' => '2018',
        'WHO_CHANGED' => '2018',
        'WHO_CREATED' => '2018',
        'MIN_BALL' => 3,
      )
    ));
	}

	public function down()
	{
    $this->dropTable('predm_ege');
		echo "m190629_082904_create_predm_ege_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
