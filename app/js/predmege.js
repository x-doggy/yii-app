/*
 * +-------------------+
 * | Useful procedures |
 * +-------------------+
 */
var isMaxLengthEnd = function ($item) {
  return $item.val().length === +$item.attr("maxlength");
};

var checkMarksRange = function ($value) {
  return $value >= "2" && $value <= "5";
};

var isMark = function ($item) {
  var $value = $($item).val();
  return $value === "" || checkMarksRange($value);
};

var isMarkActual = function ($item) {
  var $value = $($item).val();
  return $value !== "" || checkMarksRange($value);
};

var assertMarksAreCorrect = function ($marks) {
  var incorrectMarksNumbers = [];
  $($marks).each(function (i, item) {
    if (!isMark(item)) {
      incorrectMarksNumbers.push(i);
    }
  });
  if (incorrectMarksNumbers.length !== 0) {
    var incorrectMarksNumbersString = incorrectMarksNumbers.join(",");
    throw new Error("Поля оценок с номерами: " + incorrectMarksNumbersString + " некорректны. Оценка должна быть числом от 2 до 5.");
  }
};

var getNumberOfFieldsWithActualMarks = function ($marks) {
  var result = 0;
  $($marks).each(function (i, item) {
    if (isMarkActual(item)) {
      result++;
    }
  });
  return result;
};

var generateTableWithMarks = function (numbersOfMarksObj, avg) {
  var $table = $("<table/>")
          .addClass("table-modal");
  var $tr = $("<tr/>");
  var $tcell = $("<th/>", {text: "Оценка"})
          .addClass("table-modal--cell")
          .appendTo($tr);
  $tcell = $("<th/>", {text: "Количество оценок"})
          .addClass("table-modal--cell")
          .appendTo($tr);
  $tr.appendTo($table);
  
  $.each(numbersOfMarksObj, function (key, val) {
    $tr = $("<tr/>");
    $tcell = $("<td/>", {text: key})
            .addClass("table-modal--cell")
            .appendTo($tr);
    $tcell = $("<td/>", {text: val})
            .addClass("table-modal--cell")
            .appendTo($tr);
    $tr.appendTo($table);
  });
  
  $tr = $("<tr/>");
  $tcell = $("<td/>")
          .addClass("table-modal--cell")
          .attr("colspan", 2);
  $("<b/>").text("Средний балл = ")
          .appendTo($tcell)
          .after(avg);
  $tcell.appendTo($tr);
  $tr.appendTo($table); 
  
  return $table;
};

/*
 * +-----------------------+
 * | Variables definitions |
 * +-----------------------+
 */
// Elements that will be focused.
var $formNextFocuses = $(".form-next-focus");
// abitId text field.
var $formNextFocusedFirst = $(".form-next-focus--first");
// Fields of marks.
var $formNextInputs = $(".form-next-input");
// Element that will be shown or hidden.
var $toggled = $("#toggled");
// Element where average mark will be put.
var $resultContainer = $("#avg-result");
// Reference to button that calculates result.
var $btnToCalc = $("#btnToCalc");
// Reference to "Save changes" button from modal window
var $btnSaveChangesModal = $("#modal-btn--save");

/*
 * +-----------------------------------------------------------+
 * | Animated appearance block of marks after abitId is filled |
 * +-----------------------------------------------------------+
 */
$toggled.hide();
$formNextFocusedFirst.on("input", function () {
  if (isMaxLengthEnd($(this))) {
    $toggled.show("slow");
  } else {
    $toggled.hide("slow");
  }
});

/*
 * +---------------+
 * | Auto-tabindex |
 * +---------------+
 */
$formNextFocusedFirst.focus();
$formNextFocuses.each(function (i, item) {
  var $item = $(item);
  $item.on("input", function (e) {
    e.preventDefault();
    var $nextInputFocused = $($formNextFocuses.get(i + 1));
    if (isMaxLengthEnd($item)) {
      $nextInputFocused.focus();
      // Uncomment if you want next input value becomes empty.
       if ($nextInputFocused.attr("type") === "text" && $nextInputFocused.val().length !== 0) {
         $nextInputFocused.val("");
       }
    }
  });
});

/*
 * +-----------------+
 * | Form validation |
 * +-----------------+
 */
$formNextInputs.each(function (i, item) {
  var $item = $(item);
  var $trSelector = ".table-next-toggle tr:nth-child(" + i + ")";
  $item.on("blur", function (e) {
    if (!isMark($item)) {
      $($trSelector).addClass("has-error");
    } else {
      $($trSelector).removeClass("has-error");
    }
    // TODO заставить работать!!!
  });
});

/*
 * +--------------------------+
 * | Calculating average mark |
 * +--------------------------+
 */
$resultContainer.hide();

$btnToCalc.on("click", function () {
  try {
    assertMarksAreCorrect($formNextInputs);

    var sum = 0;
    var numbersOfMarks = {
      2: 0, 3: 0, 4: 0, 5: 0
    };

    $($formNextInputs).each(function (i, item) {
      var $currentValue = +$(item).val();
      numbersOfMarks[$currentValue]++;
      sum += $currentValue;
    });

    var numberOfFieldsOfActualMarks = getNumberOfFieldsWithActualMarks($formNextInputs);
    var avg = (sum / numberOfFieldsOfActualMarks).toPrecision(4);

    $resultContainer.empty();
    $( generateTableWithMarks(numbersOfMarks, avg) ).appendTo($resultContainer);
  } catch (e) {
    $resultContainer.text(e.message);
    $btnSaveChangesModal.attr("disabled", true);
    throw e;
  } finally {
    $resultContainer.show();
  }
});